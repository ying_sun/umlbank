package com.bank;

import java.math.BigDecimal;

public class Account {

    public static final String ACCOUNT_TYPE_MORTGAGE = "ACCOUNT_TYPE_MORTGAGE";
    public static final String ACCOUNT_TYPE_CHEQUING = "ACCOUNT_TYPE_CHEQUING";
    public static final String ACCOUNT_TYPE_CREDITCARD = "ACCOUNT_TYPE_CREDITCARD";

    //how to express the account only belong to one branch
    private Client client;
    private Client jointClient;
    private Branch branch;
    private String accountId;
    private BigDecimal balance;
    private BigDecimal credit; // over draft limit
    private String accountType;
    private Double interestRate;
    private BigDecimal monthlyFee;
    private String privileges;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getJointClient() {
        return jointClient;
    }

    public void setJointClient(Client jointClient) {
        this.jointClient = jointClient;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
    }

    public BigDecimal getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(BigDecimal monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public String getPrivileges() {
        return privileges;
    }

    public void setPrivileges(String privileges) {
        this.privileges = privileges;
    }

    @Override
    public String toString() {
        return "Account{" +
                ", accountId='" + accountId + '\'' +
                ", balance=" + balance +
                ", credit=" + credit +
                ", accountType='" + accountType + '\'' +
                ", interestRate=" + interestRate +
                ", monthlyFee=" + monthlyFee +
                ", privileges='" + privileges + '\'' +
                '}';
    }
}
