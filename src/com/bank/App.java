package com.bank;

import com.bank.*;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

public class App {

    public static void main(String[] args) {
        UMLBank umlBank = new UMLBank();

        // Initialize divisions
        Division planning = new Division();
        planning.setDivisionName("Planning");

        Division rrsp = new Division();
        rrsp.setDivisionName("RRSP");

        Division investments = new Division();
        investments.setDivisionName("Investments");
        investments.setSubDivision(rrsp);

        // Initialize employees
        Employee tremblay = new Employee();
        tremblay.setTitle(Employee.TITLE_MANAGER);
        tremblay.setDivision(planning);

        Employee walsh = new Employee();
        walsh.setTitle(Employee.TITLE_NON_MANAGER);
        walsh.setDivision(planning);

        Employee brown = new Employee();
        brown.setTitle(Employee.TITLE_NON_MANAGER);
        brown.setDivision(planning);

        planning.setManager(tremblay);
        planning.setEmployees(Arrays.asList(walsh, brown));

        // Initialize account
        Account callumCCAccount = new Account();
        callumCCAccount.setAccountId("40f92189-378a-40b8-89ae-4429da0d0d05");
        callumCCAccount.setAccountType(Account.ACCOUNT_TYPE_CREDITCARD);
        callumCCAccount.setBalance(new BigDecimal("10034.91"));
        callumCCAccount.setCredit(new BigDecimal("34235.08"));
        callumCCAccount.setPrivileges("Purchase insurance");
        callumCCAccount.setInterestRate(0.1);
        callumCCAccount.setMonthlyFee(new BigDecimal("4.00"));

        //added by ke
        Account johnAccount = new Account();
        johnAccount.setAccountId("50f92189-378a-40b8-89ae-4429da0d0d05");
        johnAccount.setAccountType(Account.ACCOUNT_TYPE_CHEQUING);
        johnAccount.setBalance(new BigDecimal("34000.98"));
        johnAccount.setPrivileges("Free food");
        johnAccount.setInterestRate(0.2);
        johnAccount.setMonthlyFee(new BigDecimal("10.00"));

        // Initialize clients
        Client callum = new Client();
        callum.setPersonalBank(walsh);
        callum.setAccounts(Arrays.asList(callumCCAccount));
        callumCCAccount.setClient(callum);//callum only define here

        Client robert = new Client();
        robert.setPersonalBank(brown);
        callumCCAccount.setJointClient(robert);

        Client john = new Client();
        john.setPersonalBank(brown);
        john.setAccounts(Arrays.asList(johnAccount));
        johnAccount.setClient(john);

        // Initialize branches
        Branch branch1 = new Branch();
        branch1.setAddress("10-123 1/2 MAIN STREET NW");
        branch1.setClients(Arrays.asList(callum, john));
        branch1.setUmlBank(umlBank);
        branch1.setBranchNumber("1");
        callumCCAccount.setBranch(branch1);//branch1 only define here
        johnAccount.setBranch(branch1);

        Branch branch2 = new Branch();
        branch2.setAddress("866 Tallwood Lane");
        branch2.setClients(Arrays.asList(robert));
        branch2.setUmlBank(umlBank);
        branch2.setBranchNumber("2");

        // Initialize a bank
        umlBank.setBranches(Arrays.asList(branch1, branch2));
        umlBank.setDivisions(Arrays.asList(planning, investments, rrsp));

        // print
        System.out.println(umlBank);
    }
}
