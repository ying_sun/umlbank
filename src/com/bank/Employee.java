package com.bank;

public class Employee {

    public static final String TITLE_MANAGER = "TITLE_MANAGER";
    public static final String TITLE_NON_MANAGER = "TITLE_NON_MANAGER";

    private Division division;
    private String title;
    private String name;
    private Employee manager;

    public Division getDivision() {
        return division;
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    @Override
    public String toString() {
        return "Employee{" +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", manager=" + manager +
                '}';
    }
}
