package com.bank;

import java.util.List;

public class Branch {

    private UMLBank umlBank;
    private String branchNumber;
    private String address;
    private List<Client> clients;
    private List<Account> accounts;

    public UMLBank getUmlBank() {
        return umlBank;
    }

    public void setUmlBank(UMLBank umlBank) {
        this.umlBank = umlBank;
    }

    public String getBranchNumber() {
        return branchNumber;
    }

    public void setBranchNumber(String branchNumber) {
        this.branchNumber = branchNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "Branch{" +
                ", branchNumber='" + branchNumber + '\'' +
                ", address='" + address + '\'' +
                ", clients=" + clients +
                ", accounts=" + accounts +
                '}';
    }
}
