package com.bank;

import java.util.List;

public class Client {
    //one client is possible to belong to multiple branches
    private Branch branch;
    private List<Account> accounts;
    private Employee personalBank;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Employee getPersonalBank() {
        return personalBank;
    }

    public void setPersonalBank(Employee personalBank) {
        if (personalBank == null &&
                !Employee.TITLE_NON_MANAGER.equals(personalBank.getTitle())) {
            // throw exception
            return;
        }

        this.personalBank = personalBank;
    }

    @Override
    public String toString() {
        return "Client{" +
                "branch=" + branch +
                ", accounts=" + accounts +
                ", personalBank=" + personalBank +
                '}';
    }
}
