package com.bank;

import java.util.List;

public class UMLBank {

    private List<Branch> branches;

    private List<Division> divisions;

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    public List<Division> getDivisions() {
        return divisions;
    }

    public void setDivisions(List<Division> divisions) {
        this.divisions = divisions;
    }

    @Override
    public String toString() {
        return "UMLBank{" +
                "branches=" + branches +
                ", divisions=" + divisions +
                '}';
    }
}
