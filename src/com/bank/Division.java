package com.bank;

import java.util.List;

public class Division {

    private String divisionName;
    private Division subDivision;

    private Employee manager;
    private List<Employee> employees;

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public Division getSubDivision() {
        return subDivision;
    }

    public void setSubDivision(Division subDivision) {
        this.subDivision = subDivision;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Division{" +
                "divisionName='" + divisionName + '\'' +
                ", subDivision=" + subDivision +
                ", manager=" + manager +
                ", employees=" + employees +
                '}';
    }
}
